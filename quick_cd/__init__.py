from enum import Enum

import appdirs
import argparse
import json
import sys
import os
import fcntl
import termios

app_name = 'quick-cd'
bookmarks_file_name = 'bookmarks.dat'
actionTag = 'action'
shortTag = 'short'


class Action(Enum):
    CREATE = 'create'
    DELETE = 'delete'
    LIST = 'list'
    CD = 'cd'


def set_action(args):
    if args.create:
        setattr(args, actionTag, Action.CREATE)
    elif args.delete:
        setattr(args, actionTag, Action.DELETE)
    elif args.list:
        setattr(args, actionTag, Action.LIST)
    elif args.label:
        setattr(args, actionTag, Action.CD)
    else:
        setattr(args, actionTag, Action.LIST)


def parse_arguments():
    parser = argparse.ArgumentParser()
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-c', '--create', action='store_true')
    options.add_argument('-d', '--delete', action='store_true')
    list_group = options.add_argument_group()
    list_group.add_argument('-l', '--list', action='store_true')
    list_group.add_argument('-s', '--short', action="store_true")
    parser.add_argument('label', nargs='?')
    parser.add_argument('path', nargs='?')
    args = parser.parse_args()
    set_action(args)
    if args.path is not None and args.action != Action.CREATE:
        raise Exception('[path] argument can only be used with --create')
    return args


def main():
    args = parse_arguments()
    data_dir = appdirs.user_data_dir(app_name)
    data_path = os.path.join(data_dir, bookmarks_file_name)

    if not os.path.isdir(data_dir):
        os.makedirs(data_dir)

    try:
        with open(data_path) as bookmarks_file:
            bookmarks = json.load(bookmarks_file)
    except FileNotFoundError:
        bookmarks = {}

    if args.action == Action.CD:
        if args.label not in bookmarks:
            print(f'No bookmark labeled "{args.label}"', file=sys.stderr)
            exit(1)
        try:
            for char in f'cd "{bookmarks[args.label]}"\n':
                fcntl.ioctl(0, termios.TIOCSTI, char)
        except OSError:
            print(f'cd "{bookmarks[args.label]}"\n')
    elif args.action == Action.CREATE:
        path = args.path
        if not path:
            path = os.getcwd()
        path = os.path.abspath(path)
        bookmarks[args.label] = path
        with open(data_path, 'w') as bookmark_file:
            json.dump(bookmarks, bookmark_file)
    elif args.action == Action.LIST:
        for bookmark, path in bookmarks.items():
            if args.short:
                print(bookmark)
            else:
                print(bookmark, path)
    else:
        del bookmarks[args.label]
        with open(data_path, 'w') as bookmark_file:
            json.dump(bookmarks, bookmark_file)
